CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Page Maintenance Mode module is very useful module. Site admin can put
specific page into maintenance mode. Only site admin can see maintenance page.
Using this module site admin can put multiple pages into maitenance mode and
configure maintenance page message according to their requirement.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/page_maintenance_mode

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/page_maintenance_mode


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Page Maintenance Mode module as you would normally install a
   contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Pagemaintenance > Settings
       for configuration.


MAINTAINERS
-----------

 * Kuldeep (kuldeep.me) - https://www.drupal.org/u/kuldeepme
