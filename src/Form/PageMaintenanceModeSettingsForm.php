<?php

namespace Drupal\page_maintenance_mode\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Page Maintenance Mode basic settings.
 */
class PageMaintenanceModeSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected $aliasManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, StateInterface $state, AliasManagerInterface $alias_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->state = $state;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('state'),
      $container->get('path_alias.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'page_maintenance_mode';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'page_maintenance_mode.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $settings = $this->config('page_maintenance_mode.settings');
    $options = [];
    if ($settings->get('page_url') != NULL) {
      foreach ($settings->get('page_url') as $value) {
        $options[] = $value;
      }
    }
    $form['page_maintenance_mode'] = [
      '#type'  => 'details',
      '#open'  => TRUE,
      '#title' => $this->t('Page Maintenance Mode'),
    ];
    $form['page_maintenance_mode']['page_maintenance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Put page into maintenance mode'),
      '#default_value' => $this->state->get('page_maintenance'),
    ];
    $form['page_maintenance_mode']['page_url'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'node',
      '#title' => $this->t('Page URL'),
      '#default_value' => (!empty($options) ? $this->entityTypeManager->getStorage('node')->loadMultiple($options) : []),
      '#description' => $this->t("Enter comma seprated page url."),
      '#tags' => TRUE,
    ];
    $form['page_maintenance_mode']['display_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message to display when page in maintenance mode'),
      '#description' => $this->t("@page-title use for current page title."),
      '#default_value' => (!empty($settings->get('display_message')) ? $settings->get('display_message') : ''),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $page_url = [];
    $alias = [];
    $settings = $this->configFactory->getEditable('page_maintenance_mode.settings');
    $page_maintenance = $form_state->getValue('page_maintenance');
    $page_urls = $form_state->getValue('page_url');
    if (!empty($page_maintenance)) {
      $this->state->set('page_maintenance', $page_maintenance);
      if (!empty($page_urls)) {
        foreach ($page_urls as $value) {
          foreach ($value as $target_id) {
            if (!empty($target_id)) {
              $page_url[] = $target_id;
              $alias[] = $this->aliasManager->getAliasByPath('/node/' . $target_id);
            }
          }
        }
        $settings->set('page_url', $page_url)
          ->set('page_alias', $alias)->save();
      }
      else {
        $settings->set('page_url', '')
          ->set('page_alias', '')->save();
      }
      $display_message = $form_state->getValue('display_message');
      if (!empty($display_message)) {
        $settings->set('display_message', trim($display_message['value']))->save();
      }
    }
    else {
      $this->state->set('page_maintenance', $page_maintenance);
    }
    drupal_flush_all_caches();
  }

}
